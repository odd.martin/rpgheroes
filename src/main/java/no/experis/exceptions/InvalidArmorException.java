package no.experis.exceptions;

/**
 * Hey Nick, I don't need this right? The Exception name should be explanatory right?
 * Or do you need to specify:
 * "This exception should be used whenever a Hero tries to equip an Armor piece that they don't meet the requirements for" :|
 */
public class InvalidArmorException extends Exception{
    public InvalidArmorException(String message) {
        super(message);
    }
}
