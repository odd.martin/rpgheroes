package no.experis.exceptions;

/**
 * So... Yeah... don't be a dummy, dumb dumb!
 * Have you tried to equip a Wand on a Warrior?
 * Ofc he would be mad about that, he can't hit stuff hard enough with a tiny stick like that...
 */
public class InvalidWeaponException extends Exception {
    public InvalidWeaponException (String message){
        super(message);
    }
}
