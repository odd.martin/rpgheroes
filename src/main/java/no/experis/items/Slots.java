package no.experis.items;

/**
 * This enum encapsulates all legal slots that a hero can equip.
 */
public enum Slots {
    Weapon, Head, Body, Legs
}
