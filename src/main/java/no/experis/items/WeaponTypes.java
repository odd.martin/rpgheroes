package no.experis.items;

/**
 * This enum encapsulates all legal weapon types in the game.
 */
public enum WeaponTypes {
    Axes, Bows, Daggers, Hammers, Staffs, Swords, Wands
}
