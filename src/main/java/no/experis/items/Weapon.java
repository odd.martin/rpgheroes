package no.experis.items;

/**
 * This class is an extension of the Item class which specifies that the item is a Weapon.
 * Weapons have 2 additional fields WeaponTypes, and a weaponDamage for how much the weapon increases the users damage.
 */
public class Weapon extends Item {

    protected WeaponTypes weaponType;
    protected double weaponDamage;

    public Weapon(String name, int requiredLevel, WeaponTypes weaponType, int weaponDamage) {
        super(name, requiredLevel, Slots.Weapon);
        this.weaponType = weaponType;
        this.weaponDamage = weaponDamage;
    }

    @Override
    public String toString() {
        String result = super.toString();
        result += "\n\tWeapon Type=" + weaponType +
                "\n\tWeapon Damage=" + weaponDamage;
        return result;
    }

    // Generated
    public WeaponTypes getWeaponType() {
        return weaponType;
    }

    public double getWeaponDamage() {
        return weaponDamage;
    }
}
