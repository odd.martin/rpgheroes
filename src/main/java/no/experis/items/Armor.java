package no.experis.items;

import no.experis.characters.HeroAttributes;

import java.util.Objects;

/**
 * This class is an extension of the Item class which specifies the item as a piece of Armor.
 * An armor has 2 addition fields being what ArmorType it is and what attribtues the armor Enhances.
 */
public class Armor extends Item{

    protected ArmorTypes armorType;
    protected HeroAttributes armorAttributes;

    // Constructor
    public Armor(String name, int requiredLevel, Slots slot, ArmorTypes armorType, HeroAttributes armorAttributes) {
        super(name, requiredLevel, slot);
        this.armorType = armorType;
        this.armorAttributes = armorAttributes;
    }

    // Generated
    public ArmorTypes getArmorType() {
        return armorType;
    }

    public HeroAttributes getArmorAttributes() {
        return armorAttributes;
    }

    @Override
    public String toString() {
        String result = super.toString();
        result += "\n\tArmor Type: " + armorType.toString();
        result += "\n\tArmor " + armorAttributes.toString();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Armor armor = (Armor) o;
        return armorType == armor.armorType && Objects.equals(armorAttributes, armor.armorAttributes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(armorType, armorAttributes);
    }
}
