package no.experis.items;


/**
 * This class is an abstraction of what Items should have in the game.
 * Every item should have a name, a requiredLevel and specify what slot it would occupy.
 */
public abstract class Item{

    protected String name;
    protected int requiredLevel;
    protected Slots slot;

    public Item(String name, int requiredLevel, Slots slot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }

    @Override
    public String toString() {
        return "\n\tSlot: " + slot +
                "\n\tItem Name: '" + name + '\'' +
                "\n\tRequired Level: " + requiredLevel;
    }

    // Generated

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public Slots getSlot() {
        return slot;
    }
}
