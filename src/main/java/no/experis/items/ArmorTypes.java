package no.experis.items;

/**
 * This enum encapsulates all legal armor types in the game.
 */
public enum ArmorTypes {
    Cloth, Leather, Mail, Plate
}

