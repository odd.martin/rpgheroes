package no.experis.characters;

import no.experis.items.ArmorTypes;
import no.experis.items.WeaponTypes;

/**
 * A Mage is a type of Hero, a character of this game.
 * The Constructor provides the starting level Attributes, and the LevelUp provides how much they should increase on level up.
 */
public class Mage extends Hero{

    // Constructor
    public Mage(String name) {
        super(name);
        levelAttributes = new HeroAttributes(1,1,8);
        validWeaponTypes.add(WeaponTypes.Staffs);
        validWeaponTypes.add(WeaponTypes.Wands);
        validArmorTypes.add(ArmorTypes.Cloth);
    }

    // Methods

    /**
     * A method that when a Mage levels up it will increase their attributes by the appropriate amount
     * by using the HeroAttributes addAttributes method. It will also call Hero's levelUp to increase their level.
     */
    @Override
    public void levelUp() {
        super.levelUp();
        levelAttributes = levelAttributes.addAttributes(new HeroAttributes(1,1,5));
    }

    @Override
    protected String getHeroClass(){
        return "Mage";
    }

    protected int getDamagingAttribute(){
        return totalAttributes().getIntelligence();
    }
}
