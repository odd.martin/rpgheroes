package no.experis.characters;

import no.experis.items.ArmorTypes;
import no.experis.items.WeaponTypes;

/**
 * A Ranger is a type of Hero, a character of this game.
 * The Constructor provides the starting level Attributes, and the LevelUp provides how much they should increase on level up.
 */
public class Ranger extends Hero{

    public Ranger(String name) {
        super(name);
        levelAttributes = new HeroAttributes(1,7,1);
        validWeaponTypes.add(WeaponTypes.Bows);
        validArmorTypes.add(ArmorTypes.Leather);
        validArmorTypes.add(ArmorTypes.Mail);
    }

    /**
     * A method that when a Ranger levels up it will increase their attributes by the appropriate amount
     * by using the HeroAttributes addAttributes method. It will also call Hero's levelUp to increase their level.
     */
    @Override
    public void levelUp() {
        super.levelUp();
        levelAttributes = levelAttributes.addAttributes(new HeroAttributes(1,5,1));
    }

    @Override
    protected int getDamagingAttribute() {
        return totalAttributes().getDexterity();
    }

    @Override
    protected String getHeroClass(){
        return "Ranger";
    }
}
