package no.experis.characters;

import no.experis.exceptions.InvalidArmorException;
import no.experis.exceptions.InvalidWeaponException;
import no.experis.items.*;

import java.util.*;

/**
 * This is an abstract class of a Hero. The Heroes are the characters of the game. Every new Hero needs a name, however
 * the rest of their attributes are set to default's values of level 1 with no equipment. levelAttributes and valid item types
 * are all depending on the subclass extending the Hero and will therefor be provided by them.
 */
public abstract class Hero {
    protected String name;
    protected int level;
    protected HeroAttributes levelAttributes;
    protected HashMap<Slots, Item> equipment;
    protected List<WeaponTypes> validWeaponTypes;
    protected List<ArmorTypes> validArmorTypes;

    // Constructor
    public Hero(String name) {
        this.name = name;
        this.level = 1;
        equipment = new HashMap<>();
        equipment.put(Slots.Legs, null);
        equipment.put(Slots.Body, null);
        equipment.put(Slots.Head, null);
        equipment.put(Slots.Weapon, null);
        this.validWeaponTypes = new ArrayList<>();
        this.validArmorTypes = new ArrayList<>();
    }

    // Methods

    /**
     * Method used to level up the character. This simply increases their level,
     * while each children class will be in charge of increasing their Attributes.
     */
    public void levelUp(){
        level++;
    }

    /**
     * This method takes the weapon provided and adds it to the Hero's equipment List.
     * @param weapon The weapon is the item that will be equipped by the Hero into their equipment HashMap.
     * @throws InvalidWeaponException The method will throw this exception if the weapon parameter sent
     * into this method is either of a Weapon Type that does not exist in this Hero's ValidWeaponTypes List,
     * or if the weapon has a higher requiredLevel than the Hero equipping it.
     */
    public void equipWeapon(Weapon weapon) throws InvalidWeaponException {

        if (!validWeaponTypes.contains(weapon.getWeaponType()))
            throw new InvalidWeaponException("This class can only equip " + validWeaponTypes.toString());
        else if (level < weapon.getRequiredLevel()){
            throw new InvalidWeaponException("You do not meet the level requirement for this weapon." );
        }
        else
            equipment.put(weapon.getSlot(), weapon);
    }

    /**
     * @param armor The armor is the item that will be equipped by the Hero into their equipment HashMap.
     * @throws InvalidArmorException The method will throw this exception if the armor parameter sent
     * into this method is either of an Armor Type that does not exist in this Hero's ValidArmorTypes List,
     * or if the armor has a higher requiredLevel than the Hero equipping it.
     */
    public void equipArmor(Armor armor) throws InvalidArmorException {
        if(!validArmorTypes.contains(armor.getArmorType()))
            throw new InvalidArmorException("This class can only equip " + validArmorTypes.toString());
        else if (armor.getRequiredLevel() > level)
            throw new InvalidArmorException("You do not meet the level requirement for this armor piece.");
        else
            equipment.put(armor.getSlot(), armor);
    }

    /**
     * @return This returns a HeroAttribute object with the total of all Attributes a character has.
     * This method will add all attributes that are in the Hero's armor equipment to their current levelAttributes.
     */
    public HeroAttributes totalAttributes(){
        HeroAttributes total = new HeroAttributes(0,0,0);
        total = total.addAttributes(levelAttributes);

        // This loop goes through all equipments in the Hero's map and add all attributes into the total.
        for(Map.Entry<Slots, Item> item : equipment.entrySet()){
            Item i = item.getValue();
            if(i == null || i.getSlot().equals(Slots.Weapon))
                continue;
            Armor a = (Armor) i;
            total = total.addAttributes(a.getArmorAttributes());
        }
        return total;
    }

    /**
     * @return Returns the total amount of their damaging attribute using the totalAttribute method.
     */
    protected abstract int getDamagingAttribute();

    /**
     * This should maybe just be a field instead, but as you didn't have it as one of the fields in the assignment
     * I will make it a method instead. I could ofc have just made a field called "heroClass" and have default values
     * for those in the Mage/Warrior/etc. constructor passing them on like that.
     */
    protected abstract String getHeroClass();

    /**
     * @return Returns a double with the total damage a Hero can do.
     * The calculation is done by checking it the Hero has a weapon, and multiplying this with 1% of the Hero's
     * damaging attribute. Formula looks like this: Hero damage = WeaponDamage * (1 + DamagingAttribute/100)
     */
    public double damageCalculator(){
        double damage = 0.0;

        // Checks if there is no weapon equipped, if that is the case then their base damage will just be 1, if not then it gets the weapon damage.
        if(equipment.get(Slots.Weapon) != null ){
            Weapon w = (Weapon) equipment.get(Slots.Weapon);
            damage = w.getWeaponDamage();
        }
        else
            damage = 1.0;

        damage *= (1 + (double) getDamagingAttribute() /100);
        return damage;
    }

    /**
     * @return Returns a string that contains the key fields of a Hero.
     * Name, Class, Level, Total Strength, Total Dexterity, Total Intelligence and Damage.
     */
    @Override
    public String toString() {
        HeroAttributes stats = totalAttributes();

        /* This would be if you needed to print out the equipment. I didn't notice I wasn't gonna do that
            before after I was done making it ._.

        String equipped = "\n";

        for(Map.Entry<Slots, Item> e : equipment.entrySet()){
            equipped += e.getKey() + ": " + (e.getValue() == null ? "None" : e.getValue());
            equipped += '\n';
        }
       */

        return "\nName: '" + name + '\'' +
                "\nClass: " + getHeroClass() +
                "\nLevel: " + level +
                "\nTotal Strength : " + stats.getStrength() +
                "\nTotal Dexterity : " + stats.getDexterity() +
                "\nTotal Intelligence : " + stats.getIntelligence()+
                "\nDamage: " + damageCalculator();
    }

    // Generated
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hero hero = (Hero) o;
        return level == hero.level && Objects.equals(name, hero.name) && Objects.equals(levelAttributes, hero.levelAttributes) && Objects.equals(equipment, hero.equipment) && Objects.equals(validWeaponTypes, hero.validWeaponTypes) && Objects.equals(validArmorTypes, hero.validArmorTypes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, level, levelAttributes, equipment, validWeaponTypes, validArmorTypes);
    }
}
