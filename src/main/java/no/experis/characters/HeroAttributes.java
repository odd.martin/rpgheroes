package no.experis.characters;

import java.util.Objects;

/**
 * Objects of this class will hold a set of the different kinds of stats a Hero can have.
 * Those being Strength, Dexterity and Intelligence.
 * Different HeroAttributes can be added together by using addHeroAttributes.
 */
public class HeroAttributes {
    private int strength;
    private int dexterity;
    private int intelligence;

    // Constructors
    public HeroAttributes(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    /**
     * @return Returns a String with the information of each attribute. Str, Dex and Int.
     */
    // Methods
    @Override
    public String toString() {
        return "Attributes: \n\t" +
                "Strength: " + strength +
                ", Dexterity: " + dexterity +
                ", Intelligence: " + intelligence;
    }

    /**
     * @param input is the HeroAttributes that are to be added along with the current attributes in this object.
     * @return returns a new HeroAttribute where str, dex and int are added to not override the current stats in case it is just a stat boost from a weapon.
     */
    public HeroAttributes addAttributes(HeroAttributes input){
        return new HeroAttributes(this.strength + input.getStrength(), this.dexterity + input.getDexterity(), this.intelligence + input.getIntelligence());
    }

    // Generated
    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HeroAttributes that = (HeroAttributes) o;
        return strength == that.strength && dexterity == that.dexterity && intelligence == that.intelligence;
    }

    @Override
    public int hashCode() {
        return Objects.hash(strength, dexterity, intelligence);
    }
}
