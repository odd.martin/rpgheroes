package no.experis.characters;

import no.experis.items.ArmorTypes;
import no.experis.items.WeaponTypes;

/**
 * A Rogue is a type of Hero, a character of this game.
 * The Constructor provides the starting level Attributes, and the LevelUp provides how much they should increase on level up.
 */
public class Rogue extends Hero{
    public Rogue(String name) {
        super(name);
        levelAttributes = new HeroAttributes(2,6,1);
        validWeaponTypes.add(WeaponTypes.Daggers);
        validWeaponTypes.add(WeaponTypes.Swords);
        validArmorTypes.add(ArmorTypes.Leather);
        validArmorTypes.add(ArmorTypes.Mail);
    }


    /**
     * A method that when a Rogue levels up it will increase their attributes by the appropriate amount
     * by using the HeroAttributes addAttributes method. It will also call Hero's levelUp to increase their level.
     */
    @Override
    public void levelUp() {
        super.levelUp();
        levelAttributes = levelAttributes.addAttributes(new HeroAttributes(1,4,1));
    }

    @Override
    protected int getDamagingAttribute() {
        return totalAttributes().getDexterity();
    }

    @Override
    protected String getHeroClass(){
        return "Rogue";
    }
}
