package no.experis.characters;

import no.experis.items.ArmorTypes;
import no.experis.items.WeaponTypes;

/**
 * A Warrior is a type of Hero, a character of this game.
 * The Constructor provides the starting level Attributes, and the LevelUp provides how much they should increase on level up.
 */
public class Warrior extends Hero{
    public Warrior(String name) {
        super(name);
        validWeaponTypes.add(WeaponTypes.Swords);
        validWeaponTypes.add(WeaponTypes.Axes);
        validWeaponTypes.add(WeaponTypes.Hammers);
        validArmorTypes.add(ArmorTypes.Mail);
        validArmorTypes.add(ArmorTypes.Plate);
        levelAttributes = new HeroAttributes(5,2,1);
    }

    /**
     * A method that when a Warrior levels up it will increase their attributes by the appropriate amount
     * by using the HeroAttributes addAttributes method. It will also call Hero's levelUp to increase their level.
     */
    @Override
    public void levelUp() {
        super.levelUp();
        levelAttributes = levelAttributes.addAttributes(new HeroAttributes(3,2,1));
    }

    @Override
    protected int getDamagingAttribute() {
        return totalAttributes().getStrength();
    }
    @Override
    protected String getHeroClass(){
        return "Warrior";
    }
}
