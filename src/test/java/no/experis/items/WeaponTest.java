package no.experis.items;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {

    Weapon actualWeapon;
    @BeforeEach
    void makeWeapon(){
        actualWeapon = new Weapon("Test", 1, WeaponTypes.Swords, 1);
    }

    @Test
    void constructor_validInput_WeaponNameShouldBeAsInput(){
        // Arrange
        String expected = "Test";

        // Act
        String actual = actualWeapon.name;

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void constructor_validInput_WeaponsRequiredLevelShouldBeAsInput(){
        // Arrange
        int expected = 1;

        // Act
        int actual = actualWeapon.requiredLevel;

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void constructor_validInput_WeaponSlotShouldBeWeaponDuh(){
        // Arrange
        Slots expected = Slots.Weapon;

        // Act
        Slots actual = actualWeapon.slot;

        // Assert
        assertEquals(expected, actual);
    }
    @Test
    void constructor_validInput_WeaponTypeShouldBeAsInput(){
        // Arrange
        WeaponTypes expected = WeaponTypes.Swords;

        // Act
        WeaponTypes actual = actualWeapon.weaponType;

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void constructor_validInput_WeaponDamageShouldBeAsInput(){
        // Arrange
        double expected = 1.0;

        // Act
        double actual = actualWeapon.weaponDamage;

        // Assert
        assertEquals(expected, actual);
    }
}