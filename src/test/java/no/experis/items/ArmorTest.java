package no.experis.items;

import no.experis.characters.HeroAttributes;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {

    Armor actualArmor;
    @BeforeEach
    void makeWeapon(){
        actualArmor = new Armor("Test", 1, Slots.Weapon, ArmorTypes.Cloth,  new HeroAttributes(1,1,1));
    }

    @Test
    void constructor_validInput_ArmorTypeShouldBeAsInput(){
        // Arrange
        ArmorTypes expected = ArmorTypes.Cloth;

        // Act
        ArmorTypes actual = actualArmor.armorType;

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void constructor_validInput_HeroAttributesShouldBeAsInput(){
        // Arrange
        HeroAttributes expected = new HeroAttributes(1,1,1);

        // Act
        HeroAttributes actual = actualArmor.armorAttributes;

        // Assert
        assertEquals(expected, actual);
    }
}