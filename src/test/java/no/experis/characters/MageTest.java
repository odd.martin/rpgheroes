package no.experis.characters;

import no.experis.exceptions.InvalidArmorException;
import no.experis.exceptions.InvalidWeaponException;
import no.experis.items.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {

    Hero actualHero;
    @BeforeEach
    void makeHero(){
        actualHero = new Mage("Test");
    }

    @Test
    void constructor_validInputs_HeroNameShouldBeAsInputInTheConstructor(){
        // Arrange
        String expected = "Test";

        // Act
        String actual = actualHero.name;

        // Assert
        assertEquals(expected, actual);

    }

    @Test
    void constructor_validInputs_HeroLevelShouldAlwaysStartAsLevelOne(){
        // Arrange
        int expected = 1;

        // Act
        int actual = actualHero.level;

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void constructor_validInputs_MageShouldStartWithTheSpecifiedAttributes(){
        // Arrange
        HeroAttributes expected = new HeroAttributes(1,1,8);

        // Act
        HeroAttributes actual = actualHero.levelAttributes;

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_validInput_ShouldReturnAnIntRepresentingTheHerosLevelAfterHavingLeveledUp() {
        // Arrange
        int expected = 2;

        // Act
        actualHero.levelUp();
        int actual = actualHero.level;

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_validInput_ShouldReturnTheSumOfEachAttributeAsAHeroAttributeObjectFromHavingLeveledUp(){
        // Arrange
        HeroAttributes expected = new HeroAttributes(2,2,13);

        // Act
        actualHero.levelUp();
        HeroAttributes actual = actualHero.levelAttributes;

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipArmor_validInput_ShouldHaveTheInputtedArmorInTheirEquipmentHashMapInTheAppropriateSlot(){
        // Arrange
        Armor expected = new Armor("Test", 1, Slots.Body, ArmorTypes.Cloth, new HeroAttributes(1,1,1));

        // Act
        try {
            actualHero.equipArmor(expected);
        } catch (InvalidArmorException e) {
            System.out.println(e.getMessage());
        }
        Armor actual = (Armor) actualHero.equipment.get(Slots.Body);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipArmor_invalidInput_ShouldThrowInvalidArmorExceptionFromHavingTriedToEquipAnInvalidArmorType(){
        // Arrange
        Armor testArmor = new Armor("Test", 1, Slots.Body, ArmorTypes.Leather, new HeroAttributes(1,1,1));
        String expected = "This class can only equip [Cloth]";

        // Act
        Exception exception = assertThrows(InvalidArmorException.class, () -> actualHero.equipArmor(testArmor));
        String actual = exception.getMessage();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipWeapon_validInput_ShouldHaveTheInputtedWeaponInTheirEquipmentHashMapInTheAppropriateSlot() throws InvalidWeaponException {
        // Arrange
        Weapon expected = new Weapon("Test", 1, WeaponTypes.Wands, 1);

        // Act
        actualHero.equipWeapon(expected);

        Weapon actual = (Weapon) actualHero.equipment.get(Slots.Weapon);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipWeapon_invalidInput_ShouldThrowInvalidWeaponExceptionFromHavingTriedToEquipAnInvalidWeaponType(){
        // Arrange
        Weapon testWeapon = new Weapon("Test", 1, WeaponTypes.Swords, 1);
        String expected = "This class can only equip [Staffs, Wands]";

        // Act
        Exception exception = assertThrows(InvalidWeaponException.class, () -> actualHero.equipWeapon(testWeapon));
        String actual = exception.getMessage();

        // Assert
        assertEquals(expected, actual);
    }
    @Test
    void equipWeapon_invalidInput_ShouldThrowInvalidWeaponExceptionFromHavingTriedToEquipAnInvalidLevelRequirement(){
        // Arrange
        Weapon testWeapon = new Weapon("Test", 2, WeaponTypes.Wands, 1);
        String expected = "You do not meet the level requirement for this weapon.";

        // Act
        Exception exception = assertThrows(InvalidWeaponException.class, () -> actualHero.equipWeapon(testWeapon));
        String actual = exception.getMessage();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipArmor_invalidInput_ShouldThrowInvalidArmorExceptionFromHavingTriedToEquipAnInvalidLevelRequirement(){
        // Arrange
        Armor testArmor = new Armor("Test", 2, Slots.Body, ArmorTypes.Cloth, new HeroAttributes(1,1,1));
        String expected = "You do not meet the level requirement for this armor piece.";

        // Act
        Exception exception = assertThrows(InvalidArmorException.class, () -> actualHero.equipArmor(testArmor));
        String actual = exception.getMessage();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void totalAttributes_validInput_ShouldReturnAHeroAttributeObjectWithTheSumOfTheHerosStatsWithoutArmor(){
        // Arrange
        HeroAttributes expected = new HeroAttributes(1,1,8);

        // Act
        HeroAttributes actual = actualHero.totalAttributes();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void totalAttributes_validInput_ShouldReturnAHeroAttributeObjectWithTheSumOfTheHerosStatsWithOneArmorPieceEquipped() throws InvalidArmorException {
        // Arrange
        HeroAttributes expected = new HeroAttributes(2,2,9);
        Armor testArmor = new Armor("Test", 1, Slots.Body, ArmorTypes.Cloth, new HeroAttributes(1,1,1));

        // Act
        actualHero.equipArmor(testArmor);

        HeroAttributes actual = actualHero.totalAttributes();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void totalAttributes_validInput_ShouldReturnAHeroAttributeObjectWithTheSumOfTheHerosStatsWithTwoArmorPiecesEquipped() throws InvalidArmorException {
        // Arrange
        HeroAttributes expected = new HeroAttributes(3,3,10);
        Armor testArmor = new Armor("Test", 1, Slots.Body, ArmorTypes.Cloth, new HeroAttributes(1,1,1));
        Armor testArmor2 = new Armor("Test", 1, Slots.Head, ArmorTypes.Cloth, new HeroAttributes(1,1,1));

        // Act
        actualHero.equipArmor(testArmor);
        actualHero.equipArmor(testArmor2);

        HeroAttributes actual = actualHero.totalAttributes();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void totalAttributes_validInput_ShouldReturnAHeroAttributeObjectWithTheSumOfTheHerosStatsAfterAnArmorPieceHasBeenReplaced() throws InvalidArmorException {
        // Arrange
        HeroAttributes expected = new HeroAttributes(3,3,10);
        Armor testArmor = new Armor("Test", 1, Slots.Body, ArmorTypes.Cloth, new HeroAttributes(1,1,1));
        Armor replacingTestArmor = new Armor("Test", 1, Slots.Body, ArmorTypes.Cloth, new HeroAttributes(2,2,2));

        // Act
        actualHero.equipArmor(testArmor);
        actualHero.equipArmor(replacingTestArmor);

        HeroAttributes actual = actualHero.totalAttributes();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void damageCalculator_validInputs_ShouldReturnDoubleThatIsTheirDamageWhenNotEquippedWithAWeapon(){
        // Arrange
        double expected = 1.08;

        // Act
        double actual = actualHero.damageCalculator();

        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void damageCalculator_validInputs_ShouldReturnDoubleThatIsTheirDamageWhenEquippedWithAWeapon() throws InvalidWeaponException {
        // Arrange
        double expected = 2.16;
        Weapon testWeapon = new Weapon("Test", 1, WeaponTypes.Wands, 2);

        // Act
        actualHero.equipWeapon(testWeapon);

        double actual = actualHero.damageCalculator();

        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void damageCalculator_validInputs_ShouldReturnDoubleThatIsTheirDamageAfterHavingReplacedAWeapon() throws InvalidWeaponException {
        // Arrange
        double expected = 3.24;
        Weapon testWeapon = new Weapon("Test", 1, WeaponTypes.Wands, 2);
        Weapon replacingWeapon = new Weapon("Test", 1, WeaponTypes.Wands, 3);

        // Act
        actualHero.equipWeapon(testWeapon);
        actualHero.equipWeapon(replacingWeapon);

        double actual = actualHero.damageCalculator();

        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void damageCalculator_validInputs_ShouldReturnDoubleThatIsTheirDamageWhenEquippedWithBothAPieceOfArmorAndAWeapon() throws InvalidWeaponException, InvalidArmorException {
        // Arrange
        double expected = 2.40;
        Weapon testWeapon = new Weapon("Test", 1, WeaponTypes.Wands, 2);
        Armor testArmor = new Armor("Test", 1, Slots.Body, ArmorTypes.Cloth, new HeroAttributes(10,10,12));

        // Act
        actualHero.equipWeapon(testWeapon);
        actualHero.equipArmor(testArmor);

        double actual = actualHero.damageCalculator();

        // Assert
        assertEquals(expected,actual);
    }
}