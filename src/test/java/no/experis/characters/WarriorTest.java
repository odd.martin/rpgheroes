package no.experis.characters;

import no.experis.exceptions.InvalidArmorException;
import no.experis.exceptions.InvalidWeaponException;
import no.experis.items.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {

    Hero actualHero;
    @BeforeEach
    void makeWarrior(){
        actualHero = new Warrior("Test");
    }
    @Test
    void constructor_validInputs_WarriorShouldStartWithTheSpecifiedAttributes(){
        // Arrange
        HeroAttributes expected = new HeroAttributes(5,2,1);

        // Act
        HeroAttributes actual = actualHero.levelAttributes;

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_HeroAttributeIncrease_ShouldReturnTheSumOfEachAttributeAsAHeroAttributeObjectFromHavingLeveledUp(){
        // Arrange
        HeroAttributes expected = new HeroAttributes(8,4,2);

        // Act
        actualHero.levelUp();
        HeroAttributes actual = actualHero.levelAttributes;

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipArmor_invalidInput_ShouldThrowInvalidArmorExceptionFromHavingTriedToEquipAnInvalidArmorType(){
        // Arrange
        Armor testArmor = new Armor("Test", 1, Slots.Body, ArmorTypes.Leather, new HeroAttributes(1,1,1));
        String expected = "This class can only equip [Mail, Plate]";

        // Act
        Exception exception = assertThrows(InvalidArmorException.class, () -> actualHero.equipArmor(testArmor));
        String actual = exception.getMessage();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipWeapon_invalidInput_ShouldThrowInvalidWeaponExceptionFromHavingTriedToEquipAnInvalidWeaponType(){
        // Arrange
        Weapon testWeapon = new Weapon("Test", 1, WeaponTypes.Wands, 1);
        String expected = "This class can only equip [Swords, Axes, Hammers]";

        // Act
        Exception exception = assertThrows(InvalidWeaponException.class, () -> actualHero.equipWeapon(testWeapon));
        String actual = exception.getMessage();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void damageCalculator_validInputs_ShouldReturnDoubleThatIsTheirDamageWhenNotEquippedWithAWeapon(){
        // Arrange
        double expected = 1.05;

        // Act
        double actual = actualHero.damageCalculator();

        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void damageCalculator_validInputs_ShouldReturnDoubleThatIsTheirDamageWhenEquippedWithBothAPieceOfArmorAndAWeapon() throws InvalidWeaponException, InvalidArmorException {
        // Arrange
        double expected = 2.40;
        Weapon testWeapon = new Weapon("Test", 1, WeaponTypes.Swords, 2);
        Armor testArmor = new Armor("Test", 1, Slots.Body, ArmorTypes.Plate, new HeroAttributes(15,10,10));

        // Act
        actualHero.equipWeapon(testWeapon);
        actualHero.equipArmor(testArmor);

        double actual = actualHero.damageCalculator();

        // Assert
        assertEquals(expected,actual);
    }
}