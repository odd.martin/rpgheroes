package no.experis.characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroAttributesTest {

    @Test
    void addAttributes_validInput_ShouldReturnAHeroAttributeWhereEachAttributeGotAddedToTheirCorrespondingAttribute() {
        // Arrange
        HeroAttributes actualAttribute = new HeroAttributes(1,1,1);
        HeroAttributes toBeAdded = new HeroAttributes(1,1,1);
        HeroAttributes expected = new HeroAttributes(2,2,2);

        // Act
        HeroAttributes actual = actualAttribute.addAttributes(toBeAdded);

        // Assert
        assertEquals(expected, actual);
    }
}