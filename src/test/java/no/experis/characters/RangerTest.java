package no.experis.characters;

import no.experis.exceptions.InvalidArmorException;
import no.experis.exceptions.InvalidWeaponException;
import no.experis.items.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {

    Hero actualHero;
    @BeforeEach
    void makeRanger(){
        actualHero = new Ranger("Test");
    }
    @Test
    void constructor_validInputs_RangerShouldStartWithTheSpecifiedAttributes(){
        // Arrange
        HeroAttributes expected = new HeroAttributes(1,7,1);

        // Act
        HeroAttributes actual = actualHero.levelAttributes;

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_HeroAttributeIncrease_ShouldReturnTheSumOfEachAttributeAsAHeroAttributeObjectFromHavingLeveledUp(){
        // Arrange
        HeroAttributes expected = new HeroAttributes(2,12,2);

        // Act
        actualHero.levelUp();
        HeroAttributes actual = actualHero.levelAttributes;

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipArmor_invalidInput_ShouldThrowInvalidArmorExceptionFromHavingTriedToEquipAnInvalidArmorType(){
        // Arrange
        Armor testArmor = new Armor("Test", 1, Slots.Body, ArmorTypes.Cloth, new HeroAttributes(1,1,1));
        String expected = "This class can only equip [Leather, Mail]";

        // Act
        Exception exception = assertThrows(InvalidArmorException.class, () -> actualHero.equipArmor(testArmor));
        String actual = exception.getMessage();

        // Assert
        assertEquals(expected, actual);
    }
    @Test
    void equipWeapon_invalidInput_ShouldThrowInvalidWeaponExceptionFromHavingTriedToEquipAnInvalidWeaponType(){
        // Arrange
        Weapon testWeapon = new Weapon("Test", 1, WeaponTypes.Swords, 1);
        String expected = "This class can only equip [Bows]";

        // Act
        Exception exception = assertThrows(InvalidWeaponException.class, () -> actualHero.equipWeapon(testWeapon));
        String actual = exception.getMessage();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void damageCalculator_validInputs_ShouldReturnDoubleThatIsTheirDamageWhenNotEquippedWithAWeapon(){
        // Arrange
        double expected = 1.07;

        // Act
        double actual = actualHero.damageCalculator();

        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void damageCalculator_validInputs_ShouldReturnDoubleThatIsTheirDamageWhenEquippedWithBothAPieceOfArmorAndAWeapon() throws InvalidWeaponException, InvalidArmorException {
        // Arrange
        double expected = 2.40;
        Weapon testWeapon = new Weapon("Test", 1, WeaponTypes.Bows, 2);
        Armor testArmor = new Armor("Test", 1, Slots.Body, ArmorTypes.Leather, new HeroAttributes(10,13,10));

        // Act
        actualHero.equipWeapon(testWeapon);
        actualHero.equipArmor(testArmor);

        double actual = actualHero.damageCalculator();

        // Assert
        assertEquals(expected,actual);
    }
}