package no.experis.characters;

import no.experis.exceptions.InvalidArmorException;
import no.experis.exceptions.InvalidWeaponException;
import no.experis.items.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {


    Hero actualHero;
    @BeforeEach
    void makeRogue(){
        actualHero = new Rogue("Test");
    }
    @Test
    void constructor_validInputs_RogueShouldStartWithTheSpecifiedAttributes(){
        // Arrange
        HeroAttributes expected = new HeroAttributes(2,6,1);

        // Act
        HeroAttributes actual = actualHero.levelAttributes;

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_HeroAttributeIncrease_ShouldReturnTheSumOfEachAttributeAsAHeroAttributeObjectFromHavingLeveledUp(){
        // Arrange
        HeroAttributes expected = new HeroAttributes(3,10,2);

        // Act
        actualHero.levelUp();
        HeroAttributes actual = actualHero.levelAttributes;

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipArmor_invalidInput_ShouldThrowInvalidArmorExceptionFromHavingTriedToEquipAnInvalidArmorType(){
        // Arrange
        Armor testArmor = new Armor("Test", 1, Slots.Body, ArmorTypes.Cloth, new HeroAttributes(1,1,1));
        String expected = "This class can only equip [Leather, Mail]";

        // Act
        Exception exception = assertThrows(InvalidArmorException.class, () -> actualHero.equipArmor(testArmor));
        String actual = exception.getMessage();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipWeapon_invalidInput_ShouldThrowInvalidWeaponExceptionFromHavingTriedToEquipAnInvalidWeaponType(){
        // Arrange
        Weapon testWeapon = new Weapon("Test", 1, WeaponTypes.Wands, 1);
        String expected = "This class can only equip [Daggers, Swords]";

        // Act
        Exception exception = assertThrows(InvalidWeaponException.class, () -> actualHero.equipWeapon(testWeapon));
        String actual = exception.getMessage();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void damageCalculator_validInputs_ShouldReturnDoubleThatIsTheirDamageWhenNotEquippedWithAWeapon(){
        // Arrange
        double expected = 1.06;

        // Act
        double actual = actualHero.damageCalculator();

        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void damageCalculator_validInputs_ShouldReturnDoubleThatIsTheirDamageWhenEquippedWithBothAPieceOfArmorAndAWeapon() throws InvalidWeaponException, InvalidArmorException {
        // Arrange
        double expected = 2.40;
        Weapon testWeapon = new Weapon("Test", 1, WeaponTypes.Daggers, 2);
        Armor testArmor = new Armor("Test", 1, Slots.Body, ArmorTypes.Leather, new HeroAttributes(10,14,10));

        // Act
        actualHero.equipWeapon(testWeapon);
        actualHero.equipArmor(testArmor);

        double actual = actualHero.damageCalculator();

        // Assert
        assertEquals(expected,actual);
    }
}