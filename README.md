# RPG-Heroes

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

This is a program that allows you to create heroes that can save the world!

In this program you can create heroes of different classes, such as Mage, Ranger, Rogue and Warrior. It works so that each hero class inherits from the Hero class which allows it to do all the stuff a Hero should be able to, and with this it is extremely easy to make new hero classes as the Hero class is what does all the heavy lifting. You can make equipment, armor and weapons, and equip your heroes with them to make them super buff! But be careful as all items need a level requirement and the classes can only equip certain items so be careful or errors will be thrown at you. You can also arbitrarily increase their level with levelUp(), because I didn't add any functionality to actually run the program in a meaningful way like doing combat and gaining experience points. By doing a .toString call on a hero you can see their stats. The code to show equipment is commented out because it wasn't a requirement, but if you want to see equipments too you can remove the comments in Hero at line 134. Other than that it doesn't really do a lot.     


## Table of Contents

- [Background](#background)
- [Install](#install-and-run)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [License](#license)

## Background

This program was made as an assignment in the Java-fullstack program at Noroff. The purpose of this assignment is to refresh our basic programming/Java knowledge to set a base needed to learn the more complicated stuff.  

## Install and Run

Download the files and open the project in your favorite IDE and run the Main method. If you are a wizard you can cast a forgotten spell in the console that might end up running it, but do so on your own risk.  

## Usage

There is not you can do in this program as it is all just a data structure. However, if you run the test folder then you will get a lovely message about all 45 tests have been passed. Maybe this was the part were I should have mentioned the display method like I did in the start, but you can't really "use" the program so I think this is appropriate.  

## Maintainers

[@OddM91](https://github.com/OddM91) - GitHub (because that was default in the readme)\
[@Odd.Martin](https://gitlab.com/odd.martin) - GitLab
## License

MIT © 2023 Odd Martin Kveseth
